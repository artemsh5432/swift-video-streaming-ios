//
//  RecordingViewController.swift
//  swift-ios-streaming
//
//  Created by Dmitry on 06/08/2019.
//  Copyright © 2019 Mark Knapp. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVFoundation

class RecordingViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var captureDevice : AVCaptureDevice?
    
    override func viewDidAppear(_ animated: Bool) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            
            print("captureVideoPressed and camera available.")
            
            var imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.mediaTypes = [kUTTypeMovie] as [String]
            imagePicker.allowsEditing = false
            
            imagePicker.showsCameraControls = true
            
            
            self.present(imagePicker, animated: true, completion: nil)
            
        }
            
        else {
            print("Camera not available.")
            //            println("Camera not available.")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info:NSDictionary!) {
        
        let tempImage = info![UIImagePickerController.InfoKey.mediaURL] as! NSURL?
        let pathString = tempImage?.relativePath
        self.dismiss(animated: true, completion: {})
        
        UISaveVideoAtPathToSavedPhotosAlbum(pathString!, self, nil, nil)
        
    }
    
}


